# Spark Gotchas

- [Spark Gotchas](#spark-gotchas)
    - [Spark Core](#spark-core)
    - [Spark SQL and Spark DataFrames](#spark-sql-and-spark-dataframes)
    - [Spark MLlib](#spark-mllib)
    - [PySpark](#pyspark)

## Spark Core

### Nested actions or transformations


Spark doesn't support nested distributed data structures or invoking actions or transformations inside of another action or transformation.

A common mistake is for example to try to filter one RDD using a content of an another RDD.


```scala
val data = sc.parallelize(Array("a", "b", "c", "d"))
val lookup = sc.parallelize(Array("a", "c"))

data.filter(x => lookup.filter(_ == "x").count != 0)
```

This will compile just fine but fail on runtime with an executor exception similar to this:


> Caused by: org.apache.spark.SparkException: RDD transformations and actions can only be invoked by the driver, not inside of other transformations; for example, rdd1.map(x => rdd2.values.count() * x) is invalid because the values transformation and count action cannot be performed inside of the rdd1.map transformation. For more information, see SPARK-5063.

or a `NullPointerException` in case of nested `DataFrame` operations.


Possible solutions:

- if one of the RDDs is small enough to be hanlded in memory you can `collect` it and `broadcast`:

```scala
val lookupBd = sc.broadcast(lookup.collect.toSet)
data.filter(lookupBd.value.contains _)

```

- convert `RDDs` to `PairwiseRDDs` and use `join`:

```scala
data.map((_, None)).join(lookup.map((_, None))).keys
```

- use external data storage (flat files, database) to perform lookups.


### Referencing variables inside closures




### Iterative `joins` and `unions`

#### Number of partitions

#### Long lineages


### Mutable data structures and `RDD` immutability

> RDD (...) Represents an immutable, partitioned collection of elements


### Scala REPL and case classes

## Spark SQL and Spark `DataFrames`

### Join expressions

#### Trivially true expression

#### Expressions not based on equality

#### Null safe equality

### Window functions

#### Window definitions without `ORDER BY` clause

### Predicate pushdown

### Ambiguous column names

## Spark MLlib

### Linear models

#### Intercept

#### Optimization

## PySpark

### Py4J and JVM calls

### Interpreter lifespan

### Python UDFs in Spark SQL


-------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Spark Gotchas</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://github.com/zero323/spark-gotchas" property="cc:attributionName" rel="cc:attributionURL">Maciej Szymkiewicz</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Code snippets licensed under the terms of the MIT license. See [LICENSE](https://github.com/zero323/spark-gotchas/blob/master/LICENSE) for details.

